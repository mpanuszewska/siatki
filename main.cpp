#include <iostream>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <algorithm>
#include <vector>
#include <typeinfo>
#include <chrono>

using namespace std;
using namespace std::chrono;

typedef OpenMesh::TriMesh_ArrayKernelT<> MyMesh;

int main(int argc, char *argv[])
{

    MyMesh mesh; //half-edge structure
 auto t1 = std::chrono::high_resolution_clock::now();

//Wczytanie pliku z siatką w formacie .obj
    if (!OpenMesh::IO::read_mesh(mesh, "test.obj"))
    {
      std::cerr << "read error\n";
      exit(1);
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";

    MyMesh::VertexIter          v_it, v_end(mesh.vertices_end());
    MyMesh::EdgeIter            e_it;
    MyMesh::VertexVertexIter    vv_it,vv_it2,vv_it3;
    MyMesh::VertexEdgeIter      ve_it;
    MyMesh::VertexFaceIter      vf_it;
    MyMesh::FaceFaceIter        ff_it, ff_it2;
    std::vector<MyMesh::Point> points;
    std::vector<MyMesh::Point>::iterator points_it;
    std::vector<MyMesh::FaceHandle> faces;
    std::vector<MyMesh::FaceHandle>::iterator faces_it;
    std::vector<MyMesh::EdgeHandle> edges;
    std::vector<MyMesh::EdgeHandle>::iterator edges_it;

//Wczytanie pliku z siatką w formacie .ply
//    if (!OpenMesh::IO::read_mesh(mesh, "chair.ply"))
//    {
//      std::cerr << "read error\n";
//      exit(1);
//    }



        t1 = std::chrono::high_resolution_clock::now();
////dla każdego wierzchołka 1 otoczenie
  for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it){ //iterator po wszystkich wierzchołkach
      //std::cout << "Badany: " << *v_it << "\n";

      for (vv_it=mesh.vv_iter( *v_it ); vv_it.is_valid(); ++vv_it) {        //iterator po otoczeniu wierzchołka (pierwszym)
            //std::cout << "Wierzcholek #" << *vv_it << ": " << mesh.point( *vv_it )<<"\n";
      }
  }


 t2 = std::chrono::high_resolution_clock::now();
  std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
    t1 = std::chrono::high_resolution_clock::now();

////dla każdego wierzchołka 2 otoczenie
      for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it){ //iterator po wszystkich wierzchołkach
          //std::cout << "\n" << "Badany: " << *v_it << "\n";
             for (vv_it2=mesh.vv_iter( *v_it ); vv_it2.is_valid(); ++vv_it2) {        //iterator po otoczeniu wierzchołka (pierwszym)
                    for (vv_it3=mesh.vv_iter( *vv_it2 ); vv_it3.is_valid(); ++vv_it3) { //iterator po otoczeniu wierzchołka (drugim)
                        if(std::find(points.begin(), points.end(), mesh.point(*vv_it3)) != points.end()) {
                        } else {
                            points.push_back(mesh.point(*vv_it3));
                        }
                    }
              }

             for (points_it=points.begin(); points_it!=points.end() ; ++points_it){
                 //std::cout << "( " << *points_it << " )";
             }

             points.clear();
      }

      t2 = std::chrono::high_resolution_clock::now();
        std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
        t1 = std::chrono::high_resolution_clock::now();

////wszystkie krawędzie wszystkich wierzchołków
      for (MyMesh::VertexIter v_it=mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
      {
          //std::cout << "\nAll edges for point #" << *v_it << " : ";


          for (MyMesh::VertexEdgeIter ve_it=mesh.ve_iter(v_it.handle()); ve_it; ++ve_it){
                  //std::cout << *ve_it << " ";
          }
      }
      t2 = std::chrono::high_resolution_clock::now();
        std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
        t1 = std::chrono::high_resolution_clock::now();

////wszystkie ściany wszystkich wierzchołków
      for (MyMesh::VertexIter v_it=mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it)
      {
          //std::cout << "\nAll faces for point #" << *v_it << " : ";


          for (MyMesh::VertexFaceIter vf_it=mesh.vf_iter(v_it.handle()); vf_it; ++vf_it){
              //std::cout << *vf_it << " ";
          }
      }
      t2 = std::chrono::high_resolution_clock::now();
        std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
        t1 = std::chrono::high_resolution_clock::now();

////wszystkie krawędzie wszystkich krawędzi (1 warstwa)

for (e_it=mesh.edges_begin(); e_it!=mesh.edges_end(); ++e_it){
    //std::cout << "\nEdge " << *e_it ;
    //std::cout << "\nTo ";
    for (MyMesh::VertexEdgeIter ve_it=mesh.ve_iter(mesh.to_vertex_handle(mesh.halfedge_handle(e_it,0))); ve_it; ++ve_it){
                      //std::cout << *ve_it << " ";
         }
    //std::cout << "\nFrom ";

    for (MyMesh::VertexEdgeIter ve_it=mesh.ve_iter(mesh.from_vertex_handle(mesh.halfedge_handle(e_it,0))); ve_it; ++ve_it){
                      //std::cout << *ve_it << " ";
         }

}
t2 = std::chrono::high_resolution_clock::now();
  std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
  t1 = std::chrono::high_resolution_clock::now();

////wszystkie krawędzie wszystkich krawędzi (2 warstwa)

for (e_it=mesh.edges_begin(); e_it!=mesh.edges_end(); ++e_it){
    //std::cout << "\nEdge " << *e_it ;
    //std::cout << "\nTo ";
    for (MyMesh::VertexEdgeIter ve_it=mesh.ve_iter(mesh.to_vertex_handle(mesh.halfedge_handle(e_it,0))); ve_it; ++ve_it){
        if(std::find(edges.begin(), edges.end(), *ve_it) != edges.end()) {
        } else {
            edges.push_back(*ve_it);
        }
     }

    for (MyMesh::VertexEdgeIter ve_it=mesh.ve_iter(mesh.from_vertex_handle(mesh.halfedge_handle(e_it,0))); ve_it; ++ve_it){
        if(std::find(edges.begin(), edges.end(), *ve_it) != edges.end()) {
        } else {
            edges.push_back(*ve_it);
        }
    }


    for (edges_it=edges.begin(); edges_it!=edges.end() ; ++edges_it){
        //std::cout << *edges_it;
    }

    edges.clear();

}

t2 = std::chrono::high_resolution_clock::now();
  std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
  t1 = std::chrono::high_resolution_clock::now();

////wszystkie ściany wszystkich ścian (1 warstwa)
      for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it){
          //std::cout << "\nAll 1st neighbour faces for face #" << *f_it << " : ";
          for(MyMesh::FaceFaceIter ff_it=mesh.ff_iter(f_it.handle()); ff_it; ++ff_it){
                //std::cout << *ff_it << " ";
          }

      }


      t2 = std::chrono::high_resolution_clock::now();
        std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
        t1 = std::chrono::high_resolution_clock::now();


////wszystkie ściany wszystkich ścian (2 warstwa)
    for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it){
        //std::cout << "\nAll 2st neighbour faces for face #" << *f_it << " : ";
        for(MyMesh::FaceFaceIter ff_it=mesh.ff_iter(f_it.handle()); ff_it; ++ff_it){
            for (ff_it2=mesh.ff_iter( *ff_it ); ff_it2.is_valid(); ++ff_it2) {
                if(std::find(faces.begin(), faces.end(), *ff_it2) != faces.end()) {
                } else {
                    faces.push_back(*ff_it2);
                }
            }
        }
        for (faces_it=faces.begin(); faces_it!=faces.end() ; ++faces_it){
            //std::cout << *faces_it;
        }

        faces.clear();

    }

    t2 = std::chrono::high_resolution_clock::now();
      std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
      t1 = std::chrono::high_resolution_clock::now();


////zamiana krawędzi dla wskazanej pary przyległych trójkątów wraz z odpowiednią zmianą struktury danych

MyMesh::FaceHandle face1;
MyMesh::FaceHandle face2;


for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it){
    //cout << typeid((*f_it).idx()).name() << endl;
    if((*f_it).idx() == 0) face1=*f_it;
    if((*f_it).idx() == 1) face2=*f_it;
}


for(MyMesh::FaceEdgeIter fe_it=mesh.fe_iter(face1); fe_it; ++fe_it){
    edges.push_back(*fe_it);
}

for(MyMesh::FaceEdgeIter fe_it=mesh.fe_iter(face2); fe_it; ++fe_it){
    if(std::find(edges.begin(), edges.end(), *fe_it) != edges.end()) {
        //std::cout << *fe_it;
        mesh.flip(*fe_it);
    }
}

t2 = std::chrono::high_resolution_clock::now();
  std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
  t1 = std::chrono::high_resolution_clock::now();

////określenie, czy dana siatka posiada brzeg.
for (MyMesh::HalfedgeIter h_it=mesh.halfedges_begin(); h_it!=mesh.halfedges_end(); ++h_it) {
    if(mesh.is_boundary(*h_it)){
        //std:cout<< "Ma brzeg";
        break;
    }
}

t2 = std::chrono::high_resolution_clock::now();
  std::cout << "process took: "<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";


}




