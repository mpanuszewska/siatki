
#include <chrono>

#include <vcg/complex/complex.h>
#include <vcg/complex/algorithms/update/topology.h>

#include <wrap/io_trimesh/import_obj.h>

#include <vcg/simplex/face/pos.h>

// Defining a face-vertex mesh using VCG:
class MyVertex;
class MyFace;
struct MyUsedTypes : public vcg::UsedTypes<vcg::Use<MyVertex>   ::AsVertexType,
                                           vcg::Use<MyFace>     ::AsFaceType>{};

// Vertex holds info about:                                              coordinates and       adjacent faces
class MyVertex  : public vcg::Vertex< MyUsedTypes, vcg::vertex::InfoOcf, vcg::vertex::Coord3f, vcg::vertex::VFAdj, vcg::vertex::BitFlags >{};
// Face holds info about:                                              composing vertices == adjacent vertices (the former is required if there's meant to be a V-F relationship)
class MyFace    : public vcg::Face<   MyUsedTypes, vcg::face::InfoOcf, vcg::face::VertexRef, vcg::face::VFAdj, vcg::face::FFAdjOcf, vcg::face::BitFlags >{};

class MyMesh    : public vcg::tri::TriMesh< vcg::vertex::vector_ocf<MyVertex>, vcg::face::vector_ocf<MyFace> >{};

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Specify an .obj file as the argument.\n");
        return -1;
    }

    auto time1 = std::chrono::high_resolution_clock::now();
    auto time2 = std::chrono::high_resolution_clock::now();

    // Open specified file
    time1 = std::chrono::high_resolution_clock::now();
    MyMesh mesh;
    int readMask;
    printf("Read status: %s\n", vcg::tri::io::ImporterOBJ<MyMesh>::ErrorMsg(
             vcg::tri::io::ImporterOBJ<MyMesh>::Open(mesh, argv[1], readMask)
             ));
    printf("Loaded mesh has %i vertices and %i faces\n", mesh.VN(), mesh.FN() );
    vcg::tri::UpdateTopology<MyMesh>::VertexFace(mesh);
    time2 = std::chrono::high_resolution_clock::now();
    auto timeDiff = time2 - time1;
    printf("File opened and mesh initialised in: %ld ns (%ld ms)\n\n",
           std::chrono::duration_cast<std::chrono::nanoseconds>(timeDiff).count(),
           std::chrono::duration_cast<std::chrono::milliseconds>(timeDiff).count());


    printf("Pkt. 2: \"dla każdego wierzchołka znalezienie elementów, do których należy\"\n");
    time1 = std::chrono::high_resolution_clock::now();
    int vertCount = 0;
    int faceCount;
    MyFace* face;
    // for each vertex in mesh...
    for (MyMesh::VertexIterator vertIter = mesh.vert.begin();
         vertIter != mesh.vert.end();
         ++vertIter)
    {
        printf("Vertice #%d: ", vertCount++);
        faceCount = 0;
        // for each face adjacent to the vertex...
        for(vcg::face::VFIterator<MyFace> vfIter(&(*vertIter));
            !vfIter.End();
            ++vfIter)
        {
            face = vfIter.F(); // obtain the handle to that face
            ++faceCount;
        }
        printf("%d face(s)\n", faceCount);
    }
    time2 = std::chrono::high_resolution_clock::now();
    timeDiff = time2 - time1;
    printf("Pkt. 2 finished in: %ld ns (%ld ms)\n\n",
           std::chrono::duration_cast<std::chrono::nanoseconds>(timeDiff).count(),
           std::chrono::duration_cast<std::chrono::milliseconds>(timeDiff).count());


//    printf("Pkt. 3a: \"dla każdego elementu wyznaczenie otoczenia elementów (pierwsza warstwa)\"\n");
//    time1 = std::chrono::high_resolution_clock::now();
//    // enable tracking face-face adjacency and calculate it
//    mesh.face.EnableFFAdjacency();
//    vcg::tri::UpdateTopology<MyMesh>::FaceFace(mesh);
//    // for each face in mesh...
//    for (MyMesh::FaceIterator faceIter = mesh.face.begin();
//         faceIter != mesh.face.end();
//         ++faceIter)
//    {
//        //?
//    }
//    // disable face-face tracking to free memory
//    mesh.face.DisableFFAdjacency();
//    time2 = std::chrono::high_resolution_clock::now();
//    timeDiff = time2 - time1;
//    printf("Pkt. 3a finished in: %ld ns (%ld ms)\n\n",
//           std::chrono::duration_cast<std::chrono::nanoseconds>(timeDiff).count(),
//           std::chrono::duration_cast<std::chrono::milliseconds>(timeDiff).count());


    return 0;
}

